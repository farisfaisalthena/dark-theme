import { Component } from '@angular/core';
import { accentColors } from 'src/constants/accent-colors';
import { ThemeService } from '../services/theme.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  theme$ = this.theme.get$('theme');
  accentColor$ = this.theme.get$('accentColor');
  accentColors = accentColors;

  constructor(private theme: ThemeService) { }

  changeTheme(theme: string) {
    if (this.theme.get('theme')) { // from pure
      this.theme.set('accentColor', 'blue');
    }
    this.theme.set('theme', theme);
  }

  changeAccentColor(accentColor: string) {
    this.theme.set('accentColor', accentColor);
  }


}
