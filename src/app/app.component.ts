import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ThemeService } from './services/theme.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  theme$: Observable<string>;
  accentColor$: Observable<string>;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private theme: ThemeService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.accentColor$ = this.theme.get$('accentColor');
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      if (this.platform.is('capacitor')) {
        this.theme$ = this.theme.get$('theme').pipe(
          tap(theme => {
            // TODO handle media query change
            const autoDark = theme === '' && window.matchMedia('(prefers-color-scheme: dark)').matches;
            // change status bar color
            if (autoDark || theme.includes('dark')) {
              this.statusBar.backgroundColorByHexString('#1d1b1b');
              this.statusBar.styleLightContent();
            } else {
              this.statusBar.backgroundColorByHexString('#e7e7e7');
              this.statusBar.styleDefault();
            }
          }),
        );
      } else {
        this.theme$ = this.theme.get$('theme');
      }
    });
  }
}
