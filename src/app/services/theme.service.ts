import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { distinctUntilChanged, pluck, skip } from 'rxjs/operators';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform } from '@ionic/angular';

export interface Settings {
  theme: string;
  accentColor: string;
}

/** Raw settings, how it was stored in ionic storage. */
type SettingsRaw = {
  [K in keyof Settings]: {
    data: Settings[K];
  }
}

const defaultData: Settings = {
  theme: '',
  accentColor: 'blue',
};

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  private data = new BehaviorSubject<Readonly<any>>({
    ...Object.assign({}, ...Object.entries(defaultData).map(([k, v]) =>
      ({ [k]: { epoch: 0, data: v } }))),
  });

  // deprecated way to check if settings is ready, keeping it for compatibility
  private readyPromise: Promise<void>;

  constructor(private storage: Storage, private platform: Platform, public statusBar: StatusBar) {
    this.readyPromise = this.storage.get('settings').then((raw: SettingsRaw | null) => {
      if (raw === null) { // keep default settings
        return;
      } else {
        this.data.next(raw);
      }
    });
  }

  set<K extends keyof Settings>(key: K, value: Settings[K]): void {
    if (this.data.value[key].data === value) { return; }
    this.data.next({ ...this.data.value, [key]: { data: value } });
    this.storage.set('settings', this.data.value);
  }

  get$<K extends keyof Settings>(key: K): Observable<Settings[K]> {
    return this.data.asObservable().pipe(
      pluck(key, 'data'),
      distinctUntilChanged(),
    ) as Observable<Settings[K]>;
  }

  get<K extends keyof Settings>(key: K): Settings[K] {
    return this.data.value[key].data as Settings[K];
  }

}
